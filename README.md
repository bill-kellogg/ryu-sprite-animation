# README #

Quick demo/playground for using interactive sprite animations that change on user interaction.

### How do I get set up? ###

Clone this repo and run a simple python server from the directory in terminal with the following command:
`python -m SimpleHTTPServer 8000`

You can now make Ryu Jab and Hadouken!

### If you don't want to bother with cloning ###

You can check this out on codepen here: https://codepen.io/mrwillybee/pen/gwZyQL