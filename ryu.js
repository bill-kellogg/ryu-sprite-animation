var ryu = document.getElementById('ryu')
  , interval;

function clearClasses() {
  ryu.className = "";
  clearInterval(interval);
}

function jab() {
  ryu.classList.toggle('jab');
  interval = setInterval(clearClasses, 200);
}

function hadouken() {
  ryu.classList.add('hadouken');
  interval = setInterval(clearClasses, 250);
}

document.getElementById('jab').addEventListener('click', jab)
document.getElementById('hadouken').addEventListener('click', hadouken)